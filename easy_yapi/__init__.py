"""Top-level package for easy-yapi."""
from .easy_yapi import Yapi

__author__ = """派大星"""
__email__ = 'nocoding@126.com'
__version__ = '0.1.2'

__all__ = (
    'Yapi',
)
